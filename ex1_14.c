#include <stdio.h>

#define ALPCONST 97

int main () {

    printf("Give a sentence, program will print you a histogram of the alpahabets in it\n");

    int c, n = 0, ch= 0;
    int charcount[25];
    char alpahabets[25];

    for(int i = 'a'; i < 'z'; i++) {
        charcount[n] = 0;
        alpahabets[n] = i;
    }

    while ((c = getchar()) != EOF) {

        if(c != '\n' && c != '\t' && c != ' ') {
            ch = c - ALPCONST;
            charcount[ch]++;
        }
    }

    for(int b = 0; b < 25; b++) {
        if(charcount[b] != 0) {
            putchar(alpahabets[b]);
            putchar(' ');
            for(int a = 0; a < charcount[b]; a++) {
                printf("#")
            }
            printf("\n");
        }
    }
}
