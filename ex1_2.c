 #include <stdio.h>

 int main() {

     int c;

     printf("Give a sentence. The program will print it word by word (each word one line).\n");

     while((c = getcahr()) != EOF) {

        if(c == '\n' || c == '\t' || c == '\b' || c == ' ') {
            printf("\n");
        } else {
            putchar(c);
        }

     }
 }
