//code is from Kernighan and Richie book. Idea is to test and maybe broke the program

#include <stdio.h>

#define IN 1
#define OUT 0

int main() {

    int c, nl, nw, nc, state;

    state = OUT;
    nl = nw = nc = 0;

    while((c = getchar()) != EOF) {
        ++nc;
        if(c == '\n') {
            ++nl;
        }
        if(c == ' ' || c == '\n' || c == '\t') {
            state = OUT;
        } else if (state == OUT) {
            state = IN;
            ++nw;
        }
    }
    printf("%d %d %d\n", nl, nw, nc);
}

//If you put dieresis (fro examplee ä ö) to program, it will count mor chars than one. I suppose tehy should be one char.
